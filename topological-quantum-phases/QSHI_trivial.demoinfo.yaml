title: "Prediction of topological quantum phase transitions"
logicalPath: "/data/shared/tutorialsNew/topological-quantum-phases/QSHI_trivial.bkr"
authors: ["Mera Acosta, Carlos", "Ahmetcik, Emre", "Carbogno, Christian", "Ouyang, Runhai", "Fazzio, Adalberto", "Ghiringhelli, Luca", "Scheffler, Matthias"]
editLink: "/notebook-edit/data/shared/tutorialsNew/topological-quantum-phases/QSHI_trivial.bkr"
isPublic: true
username: "tutorialsNew"
description: >
  This tutorial shows how to find descriptive parameters (short formulas) for the prediction of
  topological phase transitions. As an example, we address the topological classification of
  two-dimensional functionalized honeycomb-lattice materials, which are formally described by
  the Z2 topological invariant, i.e., Z2=0 for trivial (normal) insulators and Z2=1 for two-dimensional
  topological insulators (quantum spin Hall insulators).
  Using a recently developed machine learning based on compressed sensing, we then derive a map of
  these materials, in which metals, trivial insulators, and quantum spin Hall insulators are separated
  in different spatial domains.
  The axes of this map are given by a physically meaningful descriptor, i.e., a non-linear analytic
  function that only depends on the properties of the material's constituent atoms, but not on the properties
  of the material itself.
  The method is based on the algorithm sure independence screening and sparsifying operator (SISSO),
  which enables to search for optimal descriptors by scanning huge feature spaces.

created_at: "2017-11-13T13:56:28.375Z"
updated_at: "2017-11-13T13:56:28.375Z"
user_update: "2018-01-30"
top_of_list: false
featured: true
labels:
  category: ["Demo"]
  platform:  ["beaker"]
  language:  ["python", "javascript"]
  data_analytics_method: ["Compressed Sensing", "SISSO"]
  application_section: ["Materials property prediction"]
  application_keyword: ["Qunatum Phase", "Topological insulator", "Classification"]
  visualization: ["NOMAD viewer"]
  reference: ["https://arxiv.org/abs/1710.03319"]
