import json, yaml, glob, logging, sys

data = []
for f in glob.glob("../*/*.demoinfo.yaml"):
    try:
        with open(f) as fIn:
            d=yaml.load(fIn.read().replace('\t','        '))
        idStr = d.get('id',f[2:])
        typeStr = d.get('type',"demos")
        attribs = d.get('attributes', {})
        for k, v in d.items():
            if k not in ['attributes', 'id', 'type']:
                attribs[k] = v
        data.append({
            'id': idStr,
            'type': 'demos',
            'attributes': attribs
        })
    except:
        logging.exception("failure for file %s",f)
json.dump({'data':sorted(data, key=lambda x: json.dumps(x, sort_keys=True))}, sys.stdout, indent = 2, sort_keys=True)
