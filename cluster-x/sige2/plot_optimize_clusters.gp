set terminal png
set output "optimize_clusters.png" 
set format y "%2.1e"
set ylabel "Energy [Ha]"
set xlabel "Number of clusters"
set border linewidth 1.5
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2
set grid ytics lc rgb "#333333" lw 1 lt 0
set grid xtics lc rgb "#333333" lw 1 lt 0
plot "info_opt.out" u 11:2 w lp pointtype 6 ps 2 lc rgb "red" title "CV score",\
"info_opt.out" u 11:8 w lp pointtype 7 ps 1.4 lc rgb "blue" title "RMSE"