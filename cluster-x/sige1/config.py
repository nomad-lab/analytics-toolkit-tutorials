# ab-initio
CALCULATOR = "exciting"
EXCITINGBIN = "/home/tutorials/exciting/bin"
ENERGY_UNITS = "Ha"

# crystal
PAR_LAT_FILE = "lat.in"
MET_LAT_FILE = "lat.in.met"
VEGARDS_LAW = True

CELL_PAR_0 = [[0.0000, 5.4310, 5.4310],
              [5.4310, 0.0000, 5.4310],
              [5.4310, 5.4310, 0.0000]]

CELL_PAR_1 = [[0.0000, 5.6580, 5.6580],
              [5.6580, 0.0000, 5.6580],
              [5.6580, 5.6580, 0.0000]]


# cluster expansion
ENERGY_CE = "mixingPerSite"
#ENERGY_CE = "total"
MAX_RADIUS = 5.0
MAX_POINTS = 4  

E0 = -4623.87413289
E1 = -33537.6274125


# Ground-state search
TEMP = 1000 # temperature [K] for the Metropolis sampling
MAX_N_CONF = 300 # number of configuratons to sample after the last LND structure was found

