set terminal png
set output "gs_search.png" 
set format y "%2.1e"
set ylabel "Energy [Ha]"
set xlabel "Composition x"
set label "Si" at graph -0.01,-0.12 
set label "Ge" at graph 0.98,-0.12 
set border linewidth 1.5
set style line 1 linecolor rgb '#0060ad' linetype 1 linewidth 2
set style line 2 linecolor rgb '#dd181f' linetype 1 linewidth 2
set grid ytics lc rgb "#333333" lw 1 lt 0
set grid xtics lc rgb "#333333" lw 1 lt 0
plot "metropolis.log" w p pointtype 7 ps 0.7 lc rgb "red" title "GS search",\
"abi.dat" w p pointtype 6 ps 2 lc rgb "blue" title "Ab-initio",\
"ece.dat" u 1:2 w p pointtype 7 ps 1.4 lc rgb "blue" title "Predicted"